<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avaliacao extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($id=NULL){
		 $paginacao=$this->uri->segment(3);
		 $erro1='Você não tem permissão para acessar essa página';
		 $erro2='Você não se cadastrou nesse curso';
		 $princi['erros']='';
		 
		$logadao= $this->session->userdata('logado');
        $logadinho= $this->session->userdata('cpf');
		$princi['idt']=$paginacao;
		if(($logadao=='')||($logadinho=='')){
			
			$princi['logado']='';
			
			
			$princi['erros']=$erro1;
			//echo $princi['erros'];
			
		}
		else{
			$princi['logado']=1;
			$princi['dados2']=$this->db->query('select * from alunos where cpf="'.$logadinho.'"');

			$princi['dados']=$this->db->query('select * from avaliacao where id_aluno='.$logadinho.' and concluiu=1 and id_curso='.$paginacao);
			$afetadas2= $this->db->affected_rows();			
			
				if($afetadas2==""){
					$princi['erros']=$erro2;
				}else{
					//echo 'aqui ficará as questões';	//inicio da logica das questões
					
					$princi['questoes']=$this->db->query('select * from questionario where id_curso="'.$paginacao.'"');
				}
		}
		$this->load->view('avaliacao', $princi);
	}
	
	public function enviarresultado($id){
		
        $logadinho= $this->session->userdata('cpf');
		
		if($logadinho==''){
			
			redirect("/saladeaula/index/");	
			
		} else {
			
		$x = 1; 
		$pontuacao=0;

			while($x <= 5) {
				$marcada=$this->input->post('s'.$x);
				$confere=$this->db->query('select * from questionario where id_avaliacao='.$this->input->post('questao'.$x));
				foreach(  $confere->result() as $p):$gab= $p->correta; endforeach;

				if($gab==$marcada){
					echo 'acertou';
					$pontuacao++;
				}else{ 
					echo 'errou';
				}
				$x++;
			} 

			$this->db->where('cpf', $logadinho);
			//$this->db->delete('notas');	

			$data = array(
						'id_aluno' => $logadinho,
						'id_curso'=> $id,
						'nota' => $pontuacao,
						'data' => date('d-m-Y')
					);

			$this->db->insert('prova', $data);
		}
		redirect("/avaliacao/resultado/".$id);

	}
		public function resultado($id){
		$logadinho= $this->session->userdata('cpf');
		
		$princi['logado']='';
		if($logadinho==''){redirect("/saladeaula/index/");}else{
			$princi['logado']=1;
						$princi['dados2']=$this->db->query('select * from alunos where cpf='.$logadinho.'');
			$confere=$this->db->query('select * from prova where id_aluno='.$logadinho.' and id_curso='.$id);
			$afetadas2= $this->db->affected_rows();
			if($afetadas2==0){redirect("/saladeaula/index/");}else{
				
			$pontuacao=$this->db->query('select * from prova where id_aluno='.$logadinho.' and id_curso='.$id);	
			foreach(  $confere->result() as $p):$gab= $p->nota; endforeach;
			
			$princi['pontuacao']= $gab;
			
			$princi['aproveitamento']= ($gab*100)/5;

			if($princi['aproveitamento']<59){$princi['status']='Seu rendimento é insuficiente para requerer um certificado.<br>Por favor, refaça o teste.<br><a href="/index.php/avaliacao/index/'.$id.'"><strong>Clique para refazer.</strong></a> ';}
			if($princi['aproveitamento']>59){$princi['status']='Seu rendimento é suficiente para requerer um certificado 
																<br><br>
																<a class="btn btn-success" href="'.base_url().'index.php/saladeaula/imprimir/'.$id.'">
																	Gerar certificado
																</a>';}
			}

			
		}
		
		$this->load->view('resultado', $princi);
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */