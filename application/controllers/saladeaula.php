<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Saladeaula extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($id=0){
		
		$logado=0;

		$paginacao=$this->uri->segment(3);

		if($paginacao=='erro'){
		    $princi['erro']=1;
		}
		else{$princi['erro']='';}
		$logadao= $this->session->userdata('logado');
		$logadinho= $this->session->userdata('cpf');

		if($logadao==1){
		  $princi['logado']=1;
		  $princi['dados']=$this->db->query('select * from alunos where cpf="'.$logadinho.'"');

		    // lista os produtos//
			if($id==0){
		  	$princi['cursos']=$this->db->query('select area.id_area, area.curso, cursos.nome, cursos.id_cursos, cursos.iframe from area, cursos where area.id_area = cursos.id_area and cursos.id_cursos');
				$princi['cursei']='';
			}else{
				$princi['cursos']=$this->db->query('select * from cursos where id_area='.$id);
				foreach($princi['cursos']->result() as $cursando):
			 		$princi['cursei']=$cursando->area;
				endforeach;
			}
		}
		else{
		    $princi['logado']=0;
		}
		$this->load->view('saladeaula', $princi);
	}

	public function aula($id){
	    $paginacao=$this->uri->segment(3);

	    if($paginacao=='erro') {
      	$princi['erro']=1;
	    }
	    else{$princi['erro']='';}

	    $logadao= $this->session->userdata('logado');
	    $logadinho= $this->session->userdata('cpf');

	    if($logadao==1){
	      $princi['logado']=1;
	      $princi['dados']=$this->db->query('select * from alunos where cpf="'.$logadinho.'"');
	    }
	    else{
      	$princi['logado']=0;
	    }

	    $princi['cursando']=$this->db->query('select area.id_area, area.curso, cursos.nome, cursos.id_cursos, cursos.iframe from area, cursos where area.id_area = cursos.id_area and cursos.id_cursos ='.$id);//$id
      $this->load->view('aulas', $princi);
	}

	public function logoff(){
	    $this->session->sess_destroy();
	    redirect("/saladeaula/index/");
	}
	
	public function concluir($id){

		$logadao = $this->session->userdata('logado');
		$logadinho = $this->session->userdata('cpf');	
		
		if(($logadao=='')||( $id=='')){ 
			redirect("/saladeaula/index/");
		} 

		else {		
			$data = array(
				'id_curso' => $id,
				'id_aluno' => $logadinho
			);

			$this->db->insert('concluidos', $data);	

			redirect(base_url()."index.php/saladeaula/aula/1/".$id);

		}
	}
	
	
	public function avaliar($id){

		$logadao = $this->session->userdata('logado');
		$logadinho = $this->session->userdata('cpf');


		if(($logadao=='')||( $id=='')){
			redirect("/saladeaula/index/");

		} else {

			$data = array(
				'id_aluno' => $logadinho,
				'id_curso' => $id,
				'concluiu' => 1,
				'data' => date('d-m-Y'),
				'id_nota' => $this->input->post('optradio')
			);

			$this->db->insert('avaliacao', $data);

			redirect(base_url()."index.php/avaliacao/index/".$id);


			} 
	}
	
	public function refazerProva($id){

		$logadao = $this->session->userdata('logado');
		$logadinho = $this->session->userdata('cpf');


		if(($logadao=='')||( $id=='')){
			redirect("/saladeaula/index/");

		} else {

			redirect(base_url()."index.php/avaliacao/index/".$id);


			} 
	}
	
	public function imprimir($id){
	    $logadao = $this->session->userdata('logado');
	    $logadinho = $this->session->userdata('cpf');
		
	    if(($logadao=='')||( $id=='')){ 
			redirect("/saladeaula/index/");        
		}
	    else {			
			$data = array(
			    'id_usuario' => $logadinho,
			    'id_curso' => $id,
			    'ativo' => 1
			);

			$this->db->insert('diplomas', $data);
			
	        $princi['dados']=$this->db->query('SELECT * FROM diplomas JOIN cursos ON diplomas.id_curso = cursos.id_cursos where id_curso='.$id);
	        $princi['dados2']=$this->db->query('SELECT * FROM alunos where cpf='.$logadinho);
	        $this->load->view('diploma', $princi);
	    }
	}
	
  /*public function refazer($id) {
      $logadao = $this->session->userdata('logado');
      $logadinho = $this->session->userdata('cpf');

      if(($logadao=='')||( $id=='')){ redirect("/saladeaula/index/");  }
      else {
          $data = array( 'ativo' => 0,);
          //$this->db->where('id_user', $logadinho);
          //$this->db->where('id_curso', $id);
          $where = array('id_user ' => $logadinho , 'id_curso ' => $id);
          $this->db->where($where);
          $this->db->update('diplomas', $data);
          redirect("/saladeaula/index/");
      }
		}*/

	public function validar()	{
		$cpf = $this->input->post('cpf');
		$senha = md5($this->input->post('senha'));
		$email_res=$this->db->query('select * from alunos where cpf="'.$cpf.'" and senha="'.$senha.'"');

		$afetadas2= $this->db->affected_rows();
		echo '<br>'. $afetadas2;
		if($afetadas2==0){
	    $this->form_validation->set_message('check_database2', 'Login ou senha não encontrado ');
	    $this->form_validation->run();
	    redirect("/saladeaula/index/erro");
		}
		else{
			$newdata = array(
			    'cpf'=> $cpf ,
			    'logado'  => 1
			);

			$this->session->set_userdata($newdata);
			redirect("/saladeaula");
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
