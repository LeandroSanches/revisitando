<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$logadao= $this->session->userdata('logado');
		$logadinho= $this->session->userdata('cpf');



		if($logadao==1){
			$princi['logado']=1;
			$princi['dados']=$this->db->query('select * from alunos where cpf="'.$logadinho.'"');

		}
		else{
			$princi['logado']=0;
		}

		$this->load->view('welcome_message', $princi);
	}
	public function tecnologia()
	{
		$logadao= $this->session->userdata('logado');
		$logadinho= $this->session->userdata('cpf');



		if($logadao==1){
			$princi['logado']=1;
			$princi['dados']=$this->db->query('select * from alunos where cpf="'.$logadinho.'"');

		}
		else{
			$princi['logado']=0;
		}

		$this->load->view('tecnologia', $princi);
	}
	public function saude()
	{
		$logadao= $this->session->userdata('logado');
		$logadinho= $this->session->userdata('cpf');



		if($logadao==1){
			$princi['logado']=1;
			$princi['dados']=$this->db->query('select * from alunos where cpf="'.$logadinho.'"');

		}
		else{
			$princi['logado']=0;
		}

		$this->load->view('saude', $princi);

	}
	public function gestao()
	{
		$logadao= $this->session->userdata('logado');
		$logadinho= $this->session->userdata('cpf');



		if($logadao==1){
			$princi['logado']=1;
			$princi['dados']=$this->db->query('select * from alunos where cpf="'.$logadinho.'"');

		}
		else{
			$princi['logado']=0;
		}

		$this->load->view('gestao', $princi);
	}
	public function cadastro($id=0)
	{
		$logadao= $this->session->userdata('logado');

		echo $logadao;



		if($logadao==1){
			redirect("/saladeaula/index/".$id);

		}else{
				$this->load->view('cadastro');
		}
	}
	public function recadastro($id=0){
		$logadao= $this->session->userdata('logado');

		echo $logadao;

		if($logadao==1){
			redirect("/saladeaula/index/".$id);

		}else{
				$this->load->view('recadastro');
		}
	}
	public function revalidar(){

		$this->form_validation->set_rules('cpf', 'CPF', 'trim|required|min_length[8]|xss_clean');
		$this->form_validation->set_rules('nome', 'Seu nome', 'trim|required');
		$this->form_validation->set_rules('email', 'Seu email', 'trim|required|valid_email');
		/*$this->form_validation->set_rules('telefone', 'Seu telefone', 'trim|required');*/
		$this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('senha2', 'confirmação de senha', 'trim|required|matches[senha2]');
		$sucesso = $this->form_validation->run();
		if ($sucesso) {
			$data = array(
				'email' => $this->input->post('email'),
				'nome' => $this->input->post('nome'),
				'cpf' => $this->input->post('cpf'),
				'senha' => md5($this->input->post('senha'))
			);

			$email = $data['email'];
			$cpf = $data['cpf'];
			$senha = $data['senha'];

			$data = array('senha' => $senha,);

			$this->db->where('email', $email);
			$this->db->where('cpf', $cpf);
			$this->db->update('alunos', $data);

			redirect("/saladeaula");

		} else {
			$this->load->view("recadastro");
		}
	}

	public function validar()
	{

		$this->form_validation->set_rules('cpf', 'CPF', 'trim|required|min_length[8]|xss_clean|callback_check_database2');
		$this->form_validation->set_rules('nome', 'Seu nome', 'trim|required');
		$this->form_validation->set_rules('email', 'Seu email', 'trim|required|valid_email');
		$this->form_validation->set_rules('telefone', 'Seu telefone', 'trim|required');
		//$this->form_validation->set_rules('curso', 'Seu curso', 'trim|required');
		//$this->form_validation->set_rules('unidade', 'Sua unidade', 'trim|required');
		$this->form_validation->set_rules('curso', 'Escolha seu Curso', 'trim|required');
		$this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('senha2', 'confirmação de senha', 'trim|required|matches[senha2]');
		$sucesso = $this->form_validation->run();
		if ($sucesso) {
			$data = array(
				'email' => $this->input->post('email'),
				'telefone' => $this->input->post('telefone'),
				'nome' => $this->input->post('nome'),
				'data' => date('Y-m-d'),
				'curso' => $this->input->post('curso'),
				'cpf' => $this->input->post('cpf'),
				'senha' => md5($this->input->post('senha')),

			);

			$this->db->insert('alunos', $data);
			redirect("/saladeaula");
		} else {
			$this->load->view("cadastro");
		}




	}
	public function  check_database2($username)
	{
		$username2 = $this->input->post('cpf');
		$email_res=$this->db->query('select * from alunos where cpf="'.$username2.'"');
		$afetadas2= $this->db->affected_rows();
		if($afetadas2==0)
		{
			echo 1;
		}
		else
		{
			$this->form_validation->set_message('check_database2', 'Esse cpf  já existe ');
			return false;
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
