<!doctype html>
<html>
<head>
<?php include_once 'importacoes.php' ?>
</head>

<body>
  <?php include_once 'cabecalho.php' ?>

  <?php $logadao= $this->session->userdata('logado');?>
    <section>
      <div class="container">
        <?php foreach($cursando->result() as $cursando):?>
        <div class="row alert alert-success bv">
			<div class="col col-xs-12 col-sm-6">
				<strong>Área:</strong> <?=$cursando->curso?> <br />
			</div>
			<div class="col col-xs-12 col-sm-6">
				<strong>Curso: </strong> <?=$cursando->nome?>
			</div>
        </div>
        <div class="row">
        	<div class="col col-sm-12">
        		<div class="embed-responsive embed-responsive-16by9" style="min-height: 65rem">
        			<iframe class="embed-responsive-item" src="<?=$cursando->iframe?>"></iframe>
				</div>
			</div>
        </div>
        
      <div class="row">
     
     		
		<?php	$logadinho = $this->session->userdata('cpf');
				
				$curso=$this->uri->segment(3);
	
				$concluido=$this->db->query('SELECT id_aluno, id_curso FROM concluidos WHERE id_aluno ='.$logadinho.' and id_curso ='.$curso);				
				$concluido = $this->db->affected_rows();
				
				$avaliacao=$this->db->query('SELECT id_aluno, id_curso FROM avaliacao WHERE  id_aluno ='.$logadinho.' and id_curso ='.$curso);				
				$avaliacao = $this->db->affected_rows();				
				
				$notaProva=$this->db->query('SELECT nota, id_curso FROM prova WHERE id_aluno ='.$logadinho.' and id_curso ='.$curso);
				foreach($notaProva->result() as $notas):
					$notaProvas = $notas->nota;
				endforeach;	
										
				
			if($concluido == ''){ ?>      		
       		<form action="<?= base_url();?>index.php/saladeaula/concluir/<?=$cursando->id_cursos?>" method="post">
       			<button class="col col-sm-12 btn btn-warning btn-block concluir">Concluir Curso e Retirar Certificado </button>
       		</form>       		
       		
       		<?php } if($concluido >= 1 && $avaliacao == '') {?>
       		<div class="avaliacao">
				<div class="col col-sm-12">
					<h3 align="center">O que achou de nosso curso?</h3>
				</div>
				<div class="col col-sm-12">
					<div class="gostoudocurso a a<?=$cursos->id_cursos?>">
						<p align="center">*Antes de retirar seu certificado, avalie o curso.</p>

						<form action="<?= base_url();?>index.php/saladeaula/avaliar/<?=$cursando->id_cursos?>" method="post">
							<div class="row">
								<div class="col col-sm-12 col-sm-2 col-sm-offset-1">
									<div class="radio">
									  <label>
										<input type="radio" name="optradio" value="1">
										Ruim
									  </label>
									</div>
								</div>
								<div class="col col-sm-12 col-sm-2">
									<div class="radio">
									  <label>
										<input type="radio" name="optradio" value="2">
										Regular
									  </label>
									</div>
								</div>
								<div class="col col-sm-12 col-sm-2">
									<div class="radio">
									  <label>
										<input type="radio" name="optradio" value="3">
										Satisfatório
									  </label>
									</div>
								</div>
								<div class="col col-sm-12 col-sm-2">
									<div class="radio">
									  <label>
										<input type="radio" name="optradio" value="4">
										Bom
									  </label>
									</div>
								</div>
								<div class="col col-sm-12 col-sm-2">
									<div class="radio">
									  <label>
										<input type="radio" name="optradio" value="5">
										Execelente
									  </label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col col-sm-12">
									<button class="btn btn-block btn-success concluir"> Avaliar e Gerar Diploma </button>
								</div>						
							</div>						
						</form>
					</div>
				</div>
			</div>
     		
     		<?php } if($concluido >= 1 && $avaliacao >= 1 && $notaProvas < 4 ) {?>       		
       		<form action="<?= base_url();?>index.php/saladeaula/refazerProva/<?=$cursando->id_cursos?>" method="post">
       			<button class="col col-sm-12 btn btn-warning btn-block concluir">Fazer prova para tirar o certificado</button>
       		</form> 
      		
       		<?php } if($concluido >= 1 && $avaliacao >= 1 && $notaProvas > 3 ) {?>       		
       		<form action="<?= base_url();?>index.php/saladeaula/imprimir/<?=$cursando->id_cursos?>" method="post"><br>
       			<button class="col col-sm-12 btn btn-success btn-block concluir">Imprimir Certificado </button>
       		</form> 
       		
        </div>
        <?php } endforeach;?>
      </div>
    </section><br>
  <?php include_once 'rodape.php' ?>
</body>
</html>
