<!doctype html>
<html>
<head>
<?php include_once 'importacoes.php' ?>
<body>
  <?php include_once 'cabecalho.php' ?>
  
<section>
  <div class="container">
    <div class="row">
     
		<? if($logado==1){?>
			<h3>Sua pontuação foi de <?=$pontuacao?> acertos</h3>
			<div class="col-md-6 col-xs-12 ">
				<div id="piechart_3d" style="width: 100%; height: 300px;"></div>
			</div>
			<div class="col-md-6 col-xs-12 alert alert-info">
				<?=$status?>
			</div>
		<?}?>           

    </div>
  </div>
</section>

  <?php include_once 'rodape.php' ?>
</body>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
	
  function drawChart() {
	var data = google.visualization.arrayToDataTable([
	  ['Erros', 'Questões'],
	  ['Acertos',     <?=$pontuacao?>],
	  ['Erros',      <?=5-$pontuacao?>],
	]);

	var options = {
	  title: 'Aproveitamento',
	  is3D: true,
	};

	var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
	chart.draw(data, options);
  }
</script>
</html>