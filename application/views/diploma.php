<!doctype html>
<html>
<head>
<?php include_once 'importacoes.php' ?>
<style>
.corpo_certificado{
 width: 1000px;
 height: 675px;
 margin: 20px auto;
 text-align: center;
}
.texto_certificado{
 width: 72%; 
 margin: -400px auto auto auto; 
 line-height: 210%
}
.impr{
	margin: 10px auto;
	}
@media print {
 .impr, .n { display: none !important; } 
}
</style>

<?php 	foreach($dados->result() as $dados): endforeach;
		foreach($dados2->result() as $dados2): endforeach; 

	date_default_timezone_set('America/Sao_Paulo');
	$date = date('d-m-Y');
	
	?>
</head>

<body>
  <?php include_once 'cabecalho.php' ?>  
  
	<div class="corpo_certificado">
		<img src="<?= base_url();?>img/certificado.jpg" width="1000" height="675" alt=""/>
		<div class="texto_certificado"> 
			<p>A Pró-Reitoria de Pós-Graduação Lato Sensu e Extensão - PROPEX da Universidade do Grande Rio Prof. José de Souza Herdy - UNIGRANRIO certifica que <strong>
			<?=$dados2->nome?></strong>, concluiu o curso de exetensão: <strong> <?=$dados->nome?> </strong>, realizado em <?=$date;?></p>
			<br><br>
			<p><span style="font-weight: bold;">Duque de Caxias, <?= $date;?></span></p> 
		</div>
	</div>
	<input type="button" class="btn btn-success btn-block impr" name="imprimir" value="Imprimir Certificado" onclick="window.print();">
 
  <?php include_once 'rodape.php' ?>
</body>
</html>

