<section>
  <!--Menu da Pagina -->
  <nav class="navbar navbar-default navbar-unigranrio" style="opacity:1">
    <div class="container-fluid">
      <div class="navbar-header col-sm-4">
        <!--Botão menu-->
        <button type="button" class="navbar-toggle collapsed ico" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar traco"></span> <span class="icon-bar traco"></span> <span class="icon-bar traco"></span> </button>
        <!--Final Botão menu-->
        <div class="col-sm-2"></div>
        <div class="navbar-brand center-block  col-sm-6 ">
        	<a href="http://www.unigranrio.br/">
            	<img src="<?= base_url() ?>/img/logo.png" class="Brand" alt="Logo Unigranrio" style="height:150%;" >
            </a>
        </div>
      </div>
      <div class="collapse navbar-collapse col-md-7 pull-right" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav text-center ">
          <li><a href="<?= base_url() ?>" class="bttn-minimal bttn-md bttn-default item"> Início </a></li>
          <li><a href="http://www.unigranrio.com.br/escolas/" target="_blank" class="bttn-minimal bttn-md bttn-default item"> Escolas </a></li>
          <?php  error_reporting( 0 );
           if($logado==0){?>
                <li><a href="<?= base_url();?>index.php/saladeaula" class="bttn-minimal bttn-md bttn-default item"> Já&nbsp;inscrito </a></li>
		  <?php } 
			if($logado==1){?>
                <li><a href="<?= base_url();?>index.php/saladeaula" class="bttn-minimal bttn-md bttn-default item"> Visualizar&nbsp;Cursos </a></li>
                <li><a href="<?= base_url();?>index.php/saladeaula/logoff" class="bttn-minimal bttn-md bttn-default item"> Deslogar </a></li>
		  <?php } ?>
        </ul>
      </div>
    </div>
  </nav>
  <!--Final do Menu da Pagina-->
</section>
