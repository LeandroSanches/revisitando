<!doctype html>
<html>
<head>
<?php include_once 'importacoes.php' ?>
</head>

<body>
<?php include_once 'cabecalho.php' ?>

<div class="container">
  <div class="row">

    <?php if($logado==1){?>
        <div class="alert alert-success text-center bv">
            Seja Bem-Vindo.
        </div>

        <h2 class="text-center escolha">Escolha o curso que deseja fazer:</h2>


        <?php foreach($cursos->result() as $cursos):?>
          <div class="col-xs-6 col-sm-3">
            <div  class="alert alert-warning cursos">
              <div class="row">
                <div class="col-xs-12 area" >
                  <B>Área:</B> <?=$cursos->curso?>
                </div>
                <div class="col-xs-12 nomecurso" >
                  <B>Curso:</B> <?=$cursos->nome?>
                </div>
                <div class="col-xs-12">
                  <a class="btn bttn-pill bttn-sm btn-block" href="<?=base_url();?>index.php/saladeaula/aula/<?=$cursos->id_cursos?>">Acessar</a>                  
                </div>                               
              </div>
            </div>
          </div>
        <?php endforeach;?>

    <?php }else{?>
      <div class="formulario">
        <?php if($erro==1){?>
            <div class="alert alert-danger"> Senha ou Login inválidos</div>
        <?php }?>

        <?php $attributes = array('class' => 'form-horizontal', 'id' => 'myform'); ?>
        <?php echo form_open("saladeaula/validar"); ?>
              <h2 class="form-signin-heading text-center">Login</h2>
              <label for="inputEmail" class="">CPF</label><!--sr-only-->
              <input name="cpf" type="text" id="cpf" class="form-control" placeholder="Coloque seu CPF" required autofocus>
              <label for="inputPassword" class="">Senha</label><!--sr-only-->
              <input type="password" id="senha" class="form-control" name="senha" placeholder="Senha" required><br>
              <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
              <a href="<?= base_url();?>index.php/welcome/cadastro" class="btn btn-lg  btn-info btn-block">Cadastrar</a>
              <a style="margin-top:20px;" href="<?= base_url();?>index.php/welcome/recadastro" type="submit">Esqueci Minha Senha</a>
        <?php echo form_close(); ?>
      </div>
    <?php }?>

  </div>
</div>

<?php include_once 'rodape.php' ?>
</body>
</html>