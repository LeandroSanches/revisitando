
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Revisitando - Unigranrio</title>

<link rel="icon" href="<?= base_url();?>img/GloboUnigranrio.png">

<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/bttn.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/estilo.css" >
<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/estilo2.css">

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Catamaran" rel="stylesheet">

<script src="<?= base_url();?>js/jquery.min.js"></script>
<script src="<?= base_url();?>js/bootstrap.min.js"></script>
<script src="<?= base_url();?>js/site.js"></script>
<script src="<?= base_url();?>js/site2.js"></script>
