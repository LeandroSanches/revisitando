 <!doctype html>
<html>
<head>
<?php include_once 'importacoes.php' ?>
</head>

<body>
  <?php include_once 'cabecalho.php' ?>
<section>
  <div class="container">
    <div class="row">
		<h1 class="tituto_chamada">AVALIAÇÃO</h1>    
		<h2>Antes de imprimir o certificado vamos avaliar seu conhecimento</h2>
		<?php if($erros==''){?>
		<form method="post" action="<?= base_url();?>index.php/avaliacao/enviarresultado/<?=$idt?>">   
        <?php	$s=0;
				foreach($questoes->result() as $pessoal):
				$s++; ?>
            <div class="alert alert-info">    
    			<strong><?=$pessoal->pergunta;?></strong><br>   <br>			 
				<input type="radio" name="s<?=$s?>" value="a"> <?=$pessoal->resp1;?><br>
				<input type="radio" name="s<?=$s?>" value="b"> <?=$pessoal->resp2;?><br>
				<input type="radio" name="s<?=$s?>" value="c"> <?=$pessoal->resp3;?><br>
				<input type="radio" name="s<?=$s?>" value="d"> <?=$pessoal->resp4;?><br>
				<input type="radio" name="s<?=$s?>" value="e"> <?=$pessoal->resp5;?><br>
				<input type="hidden" name="questao<?=$s?>" value="<?=$pessoal->id_avaliacao;?>">
         	</div>
          <?php endforeach; ?>
             <input type="submit" style=" margin-bottom:20px;" class="btn btn-info" value="Enviar respostas">
         </form>
         <?php }else{?>    
			<div class="col-md-12 col-xs-12">
			   <div class="alert alert-danger"><?=$erros?></div>
			</div> 
		<?php }?>
    </div>
  </div>
</section>


  <?php include_once 'rodape.php' ?>
</body>
</html>
