<section>
  <div>
    <div class="col-md-12 center-block navbar-rodape n" style="opacity:1;">
      <div class="row">
        <div class="col-xs-12 col-md-3"> <img src="<?= base_url();?>/img/logo.png" class="img-responsive center-block img-rodape" alt="Logo Unigranrio" > </div>
        <div class="col-md-6 col-xs-12">
          <h2 class="text-center text-center">UNIDADES</h2>
          <div class="col-md-6 col-xs-12">
            <ul class="unidades sem-margem" type="none">
              <li>Rio de Janeiro - RJ</li>
              <ul class="unidades">
                <li><a href="http://www2.unigranrio.br/unidades/duque-de-caxias.php" target="_blank"><i>Campus I </i>- Duque de Caxias</a></li>
                <li><a href="http://www2.unigranrio.br/unidades/lapa.php" target="_blank"><i>Campus II</i> - Lapa</a></li>
                <li><a href="http://www2.unigranrio.br/unidades/silva-jardim.php" target="_blank"><i>Campus III</i> - Silva Jardim</a></li>
                <li><a href="http://www2.unigranrio.br/unidades/mage.php" target="_blank"><i>Campus IV</i> - Magé</a></li>
                <li><a href="http://www2.unigranrio.br/unidades/sao-joao-de-meriti.php" target="_blank"><i>Campus V</i> - São João de Meriti</a></li>
                <li><a href="http://www2.unigranrio.br/unidades/mage.php" target="_blank"><i>Campus VI</i> - Macaé</a></li>
                <li><a href="http://www2.unigranrio.br/unidades/nova-iguacu.php" target="_blank"><i>Campus VII</i> — Nova Iguaçu</a></li>
                <li><a href="http://www2.unigranrio.br/unidades/barra-da-tijuca.php" target="_blank">Unidade Barra da Tijuca</a></li>
                <li><a href="http://www2.unigranrio.br/unidades/carioca-shopping.php" target="_blank">Unidade Carioca Shopping</a></li>
                <li><a href="http://www2.unigranrio.br/unidades/penha.php" target="_blank">Unidade Penha</a></li>
                <li><a href="http://www2.unigranrio.br/unidades/santa-cruz-da-serra.php" target="_blank">Unidade Santa Cruz da Serra</a></li>
              </ul>
            </ul>
          </div>
          <div class="col-md-6 col-xs-12">
            <ul class="unidades sem-margem" type="none">
              <li>Palhoça - SC</li>
              <ul class="unidades">
                <li><a href="http://www2.unigranrio.br/unidades/fatenp.php" target="_blank">Unidade Fatenp</a></li>
              </ul>
              <br>
              <li>Grupo Unigranrio</li>
              <ul class="unidades">
                <li><a href="http://www.capunigranrio.com.br/matriculas/" target="_blank">CAP - Colégio de Aplicação Unigranrio</a></li>
                <li><a href="http://pdcsaude.com.br/" target="_blank">PDC - Policlínica de Saúde</a></li>
              </ul>
            </ul>
          </div>
        </div>
        <div class="col-md-3 col-xs-12">
          <div class="redes center-block">
              <a href="https://www.facebook.com/unigranrio" target="_blank">
              	<img class="icon icon_face" src="<?= base_url();?>/img/icon_face.jpg" alt=""/>
              </a>
              <a href="https://plus.google.com/u/0/109177974170765826768" target="_blank">
              	<img class="icon icon_gp" src="<?= base_url();?>/img/icon_gp.jpg" alt=""/>
              </a>
              <a href="https://twitter.com/unigranrio" target="_blank">
              	<img class="icon icon_tw" src="<?= base_url();?>/img/icon_tw.jpg" alt=""/>
              </a>
              <a href="https://www.youtube.com/user/unigranrioonline" target="_blank">
              	<img class="icon icon_yt" src="<?= base_url();?>/img/icon_yt.jpg" alt=""/>
              </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="col-md-12 center-block navbar-rodape2 n" style="opacity:1;">
    <div class="row">
      <div class="col-md-6 text-center"> © 2017. Todos os direitos reservados à UNIGRANRIO. </div>
      <div class="col-md-6 text-center">
        <ul type="none" class="list-inline">
          <li> <a href="tel:+552132194040" title="Clique aqui para ligar"><img src="<?= base_url();?>/img/icon-tel.png" width="20" alt=""> (21) 3219-4040</a> </li>
          <li> <a href="http://www2.unigranrio.br/possoajudar/" target="_blank" title="Posso Ajudar?">POSSO AJUDAR</a> </li>
          <li> <a href="http://uniatendimento.unigranrio.edu.br/crm/paginaFaleConosco.jsf" target="_blank">FALE CONOSCO</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
