<!DOCTYPE html>
<html>
<head>
<?php include 'importacoes.php' ?>
</head>
<body>
<?php include_once 'cabecalho.php' ?>
<section class="section-formulario">
<?php $attributes = array('class' => 'form-horizontal', 'id' => 'myform'); ?>

<?php echo form_open("welcome/revalidar"); ?>
  <div class="container">
    <div class="row div-formulario">
    	<? if(validation_errors()==''){}

    else{
    	?>
        <div class="alert alert-danger div-erro" role="alert"> <?php echo validation_errors(); ?> </div>
<? }?>
      <h1 class="tituto_chamada">PREENCHA SEUS DADOS</h1>
      <div class="form-cadastro col-md-12  ">
        <form>
          <div class="form-group">
            <label for="exampleInputEmail1">Nome</label>
            <input value="<?=set_value("nome","")?>" type="text" class="form-control" name="nome" id="nome" placeholder="Nome" autofocus>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">E-mail</label>
            <input value="<?=set_value("email","")?>" type="email" class="form-control" name="email"  id="email" placeholder="E-mail">
          </div>
          <!--<div class="form-group">
            <label for="telefone">Telefone </label>
            <input value="<?=set_value("telefone","")?>" type="tel" class="form-control"  name="telefone" id="telefone" placeholder="Telefone">
          </div>-->
          <div class="form-group">
            <label for="cpf">CPF (Digite apenas números) </label>
            <input type="text" name="cpf" value="<?=set_value("cpf","")?>" title="Digite o CPF" class="form-control" id="cpf" placeholder="CPF">
          </div>
          <div class="form-group">
            <label for="senha">Nova Senha</label>
            <input type="password"  name="senha" class="form-control" id="senha" placeholder="Nova Senha">
          </div>
          <div class="form-group">
            <label for="senhac">Confirmar Nova Senha</label>
            <input type="password"  name="senha2" class="form-control" id="senhac" placeholder="Confirmar Nova Senha">
          </div>
          <button type="submit" class="btn btn-success conversao btn-block">Alterar Senha</button>
          <?php echo form_close(); ?>
        </form>
      </div>
    </div>
  </div>
</section>
<!--application/views/rodape.php-->
<?php include_once 'rodape.php' ?>
</body>
</html>
