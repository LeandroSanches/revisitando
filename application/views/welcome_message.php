<!doctype html>
<html>
<head>
<?php include_once 'importacoes.php' ?>
</head>

<body>
<!--application/views/cabecalho.php-->
<?php include_once 'cabecalho.php' ?>

<section class="n">
  <!--Partes das imgs -->
  <div class="navbar nav-img" style="opacity:1;">
    <div class="col-md-6 div-img"> <img src="img/dreamstime_xl_6547980.png" class="img-responsive img1" alt=""/>
      <div class="txt text-right">
        <h2>Licenciando</h2>
        <ul type="none">
          <li>Experiência em tutoria e/ou mentoria;</li>
          <li>Prática no uso de metodologias ativas;</li>
          <li>Vivência em projetos educacionais em espaços não formais;</li>
          <li>Pontuação no processo seletivo para docência, tutoria e preceptoria na Unigranrio.</li>
        </ul>
      </div>
    </div>
    <div class="col-md-6 div-img"> <img src="img/dreamstime_xl_26272062.png" class="img-responsive img1" alt=""/>
      <div class="txt2">
        <h2>Ingressante</h2>
        <ul class="sem-margem" type="none">
          <li>Carga horária de ACC;</li>
          <li>Proximidade com a Unigranrio;</li>
          <li>Melhoria do desempenho acadêmico;</li>
          <li>Reforço/revisão de conhecimentos básicos e imprescindíveis ao ingresso no curso.</li>
        </ul>
      </div>
    </div>
  </div>
  <!--Final da Partes das imgs -->
</section>
<section>
    <div class="col-md-12 navbar navbar-apresentacao" style="opacity:1;">
      <div class="center-block text-center">
        <h1><strong class="strong-borda">Conheça a Nossa Metodologia</strong></h1>
      </div>
      <div class="col-md-12">
        <div class="embed-responsive-item">
          <video width="80%" class="center-block" controls>
            <source src="video/acolhimento-2016-02.mp4" type="video/mp4">
            </source>
            Seu navegador não suporta essa tecnologia! </video>
        </div>
        <div class="center-block video-texto">
          <p>O Revisitando foi criado por iniciativa dos Cursos de Licenciatura da Unigranrio, em parceria com a Pró-reitora de Graduação (PROGRAD). Este programa visa criar ações que integrem os Cursos de Licenciatura da Unigranrio, os profissionais em formação (em nível de estágio supervisionado - Licenciando), os profissionais que acabaram de entrar nestas Licenciaturas (Ingressantes) e os Professores/Preceptores destes cursos. As ações integradas decorrentes deste programa serão norteadas pelo Novo Modelo de Ensino da Unigranrio e permitirão o cumprimento de um currículo baseado em competências e vivências profissionais e pessoais, por meio da utilização de ferramentas de metodologias ativas, Tecnologias da Informação e Comunicação (TIC's) e outros processos vencedores e inovadores. Assim, esperamos dar uma maior e melhor vivência do mundo do trabalho aos Licenciandos; e aos Ingressantes, acolhê-los e melhor prepará-los para a vivência acadêmico-profissional.</p>
        </div>
      </div>
    </div>
</section>
<section>
  <div class="">
    <div class="col-md-12 navbar navbar-área" style="opacity:1;">
      <div class="center-block text-capitalize apresentacao">
        <h1><strong class="strong-borda texto-area">conheça nossas áreas</strong></h1>
      </div>
      <div class="row">
        <div class="col-sm-1"></div>
        <a href="#ciencia">
        <div class="col-xs-6 col-sm-2 poiter prof prof5" data-exibir="5"><img src="img/Ativo_5.png" class="ativo ativo5" width="100%" alt=""/>
          <div class="center-block text-center texto-cor prof prof5">
            <h3><strong>Ciência e Biologia</strong></h3>
          </div>
        </div>
        </a> <a href="#historia">
        <div class="col-xs-6 col-sm-2 poiter prof prof1" data-exibir="1"><img src="img/Ativo_1.png" class="ativo ativo1" width="100%" alt="" data-exibir="1"/>
          <div class="center-block text-center text-capitalize texto-cor prof prof1">
            <h3><strong>História</strong></h3>
          </div>
        </div>
        </a> <a href="#matematica">
        <div class="col-xs-6 col-sm-2 poiter prof prof3" data-exibir="3"><img src="img/Ativo_3.png" class="ativo ativo3" width="100%" alt=""/>
          <div class="center-block text-center text-capitalize texto-cor prof prof3">
            <h3><strong>Matemática</strong></h3>
          </div>
        </div>
        </a> <a href="#portugues">
        <div class="col-xs-6 col-sm-2 poiter prof prof4" data-exibir="4"><img src="img/Ativo_4.png" class="ativo ativo4" width="100%" alt=""/>
          <div class="center-block text-center texto-cor prof prof4">
            <h3><strong>Português e Inglês</strong></h3>
          </div>
        </div>
        </a> <a href="#quimica">
        <div class="col-xs-6 col-sm-2 poiter prof prof2" data-exibir="2"><img src="img/Ativo_2.png" class="ativo ativo2" width="100%" alt=""/>
          <div class="center-block text-center text-capitalize texto-cor prof prof2">
            <h3><strong>Química</strong></h3>
          </div>
        </div>
        </a>
        <div class="col-sm-1"></div>
      </div>
      <div class="row">
        <div class="col-sm-1"></div>
        <div id="historia" class="col-sm-10 text-justify caixa-text  f1 intern1">
          <p>Conheça a área de História</p>
          <br>
          <p>O Curso de História da Universidade do Grande Rio Prof. José de Souza Herdy está voltado para a formação profissional do Historiador. Por isso, mantém a especificidade do campo de formação e trabalho do profissional da área de História, capacitando-o para lidar com as temporalidades e com uma exigência específica e formativa a um trabalho com variadas fontes documentais, respeitando em cada caso, os elementos e características sociais, políticas, econômicas e culturais de cada época e contexto.</p>
          <p>Preocupados com a formação do Historiador, o curso e os profissionais envolvidos mantêm o foco na trabalhabilidade, habilitando o profissional para o mercado de trabalho, com foco na pesquisa e na docência da História. Por isso, por meio da utilização de ferramentas de pesquisa, de manipulação e análise de documentos históricos, fundamentados nas teorias e métodos da História, assim como a utilização de metodologias ativas e recursos tecnológicos integrados (TIC's), objetivamos capacitar o profissional em formação para o exercício do ofício do historiador.</p>
          <p>O Revisitando integra o aluno ao Laboratório de História, que é dividido em quatro áreas: História Antiga e Medieval, História Moderna e Contemporânea, História das Américas e História das Culturas, com o objetivo de inseri-lo na pesquisa histórica.</p>
          <p>Por meio do Projeto de Produção Histórica para as Mídias, o aluno desenvolve o trabalho de pesquisa voltado para rádio, televisão e mídia impressa.</p>
          <p>E por meio do Revisitando, os alunos que concluíram a metade do curso auxiliam os ingressantes a integrarem-se no Curso de História, desenvolvendo trabalhos de monitoria.</p>
          <a href="<?= base_url();?>index.php/welcome/cadastro">
          <div class="text-center pull-right bttn-slant bttn-md bttn-primary cor">Inscreva-se Grátis</div>
          </a> </div>
        <div id="quimica" class="col-sm-10 text-justify caixa-text f2 intern1">
          <p>Conheça a área de Química</p>
          <p>A Química é uma ciência que está nos produtos consumidos, em medicamentos e tratamentos médicos, na alimentação, nos combustíveis, na geração de energia, na tecnologia, no meio ambiente, na vida e nas consequências para a economia. Portanto, exige-se que o cidadão tenha o mínimo de conhecimento químico para poder participar da sociedade tecnológica atual. Trata-se de formar o cidadão para sobreviver e atuar de forma responsável e comprometida na sociedade científico-tecnológica, na qual a Química aparece como relevante instrumento de investigação, produção de bens e desenvolvimento socioeconômico, interferindo diretamente no cotidiano das pessoas. O aprendizado visa auxiliar na busca de novos conhecimentos, partindo do que é conhecido, e fazendo a relação entre as outras áreas do conhecimento, a fim de buscar respostas às dúvidas que surgem na sua vida, bem como aquelas relacionadas aos conteúdos específicos da disciplina.</p>
          <p>Os Parâmetros Curriculares Nacionais orientam para o desenvolvimento que contemple a interdisciplinaridade como algo que vai além da justaposição de disciplinas e que, ao mesmo tempo, evita a diluição do conhecimento.</p>
          <p>A área de Química do Revisitando irá atuar nas competências iniciais para o ingressante, de maneira a integrá-lo com o Licenciando em Química, fazendo uso das Metodologias Ativas, TIC's e laboratórios. Há ainda, a oportunidade de integração com as outras áreas do Revisitando no mesmo espaço pedagógico.</p>
          <a href="<?= base_url() ?>index.php/welcome/cadastro">
          <div class="text-center pull-right bttn-slant bttn-md bttn-primary cor">Inscreva-se Grátis</div>
          </a> </div>
        <div id="matematica" class="col-sm-10 text-justify caixa-text f3 intern1">
          <p>Conheça a área de Matemática</p>
          <p>Na área de Matemática, habilidades e competências inerentes à formação do professor, tais como: raciocínio lógico, concentração, postura crítica, capacidade de formular hipóteses e de resolver situações-problemas, tornam o profissional capaz de ocupar posições em escolas dos ensinos Fundamental e Médio, da rede pública ou privada, de qualquer lugar do país. Desta maneira, é preciso trabalhar para o desenvolvimento de um docente que tenha conhecimento amplo da Matemática, uma sólida formação pedagógica e que esteja disposto a estudar questões referentes a uma aprendizagem continuada, utilizando outras metodologias e tecnologias. Com base nas Diretrizes Curriculares Nacionais, este profissional poderá atuar em programas educacionais, organizar e dinamizar projetos e atividades didáticas multidisciplinares, estabelecer relações entre a Matemática e outras áreas do conhecimento, além de desenvolver pesquisas científicas nessa área. Assim, o formando em Matemática deverá ter a capacidade de: expressar-se com clareza, precisão e objetividade; compreender e utilizar conhecimentos matemáticos; trabalhar em equipes e exercer liderança; avaliar livros-texto e estruturar cursos e tópicos de ensino de Matemática. </p>
          <p>A área de Matemática do Revisitando irá atuar junto aos ingressantes, integrando-os com o curso de Matemática, por meio do uso de metodologias ativas, TIC's e espaços laboratoriais, além de efetuar a ligação com as outras áreas do Revisitando no mesmo espaço pedagógico.</p>
          <a href="<?= base_url() ?>index.php/welcome/cadastro">
          <div class="text-center pull-right bttn-slant bttn-md bttn-primary cor">Inscreva-se Grátis</div>
          </a> </div>
        <div id="portugues" class="col-sm-10 text-justify caixa-text  f4 intern1">
          <p>Conheça a área de Português e Inglês</p>
          <p>O curso de Letras da Unigranrio tem como compromisso a formação inicial de profissionais que sejam capazes de contribuir para a construção de uma sociedade mais justa e tolerante à diversidade. O campo de atuação permite que, além de professor de línguas e/ou literaturas, o atuante possa exercer a função de professor em escolas de idiomas, atuar como redator, revisor de textos, tradutor e intérprete. Busca-se formar um profissional crítico que trabalhe as diferentes linguagens, em especial, a verbal em suas modalidades oral e escrita.</p>
          <p>Segundo as Diretrizes Curriculares Nacionais, os objetivos do ensino superior de Letras são: formar profissionais interculturalmente competentes, a fim de lidar com as linguagens; refletir criticamente sobre tais linguagens; utilizar as novas tecnologias e fazer com que o discente enxergue a sua formação profissional como um processo contínuo. Além disso, o Curso de Letras contribui para o desenvolvimento de conhecimentos práticos e intertextuais, que viabilizem as necessidades da vida contemporânea, e também os conhecimentos mais amplos, possibilitando uma maior visão de mundo, bem como a sua cultura geral.</p>
          <p>O Revisitando do Curso de Letras se concentra em duas áreas de conhecimento específico: Língua Portuguesa e Língua Inglesa. O seu principal objetivo é desenvolver competências iniciais, de modo que o aluno possa aplicá-las na prática de lecionar, ainda no espaço acadêmico, sob a orientação dos docentes do curso, fazendo uso das metodologias ativas e TIC's.</p>
          <a href="<?= base_url() ?>index.php/welcome/cadastro">
          <div class="text-center pull-right bttn-slant bttn-md bttn-primary cor">Inscreva-se Grátis</div>
          </a> </div>
        <div id="ciencia" class="col-sm-10 text-justify caixa-text  f5 intern1">
          <p>Conheça a área de Ciências e Biologia</p>
          <p>Na área de Ciências e Biologia, é crescente a valorização do conhecimento e da capacidade de inovar demandas, além de formar cidadãos capazes de aprender continuamente o que é essencial para uma formação geral, e não apenas um treinamento específico. O aprendizado deve contribuir não só para o conhecimento técnico, mas, também, para uma cultura mais ampla, desenvolvendo meios para a interpretação de fatos naturais e compreendendo procedimentos e equipamentos do cotidiano social e profissional.</p>
          <p>Com foco nas Diretrizes Curriculares Nacionais, e segundo os Parâmetros Curriculares Nacionais, os objetivos do Ensino Médio em cada área do conhecimento devem envolver, de forma combinada, o desenvolvimento de conhecimentos práticos, contextualizados, que respondam às necessidades da vida contemporânea e o desenvolvimento de conhecimentos mais amplos e abstratos, que correspondam a uma cultura geral e a uma visão de mundo.</p>
          <p>A área de Ciências e Biologia do Revisitando irá atuar nas competências iniciais para os ingressantes, facilitando a relação de ensino-aprendizagem dentro das Unidades Curriculares. Os ingressantes assumirão uma relação de ensino formal, em contraturno, com os Licenciandos, sob a supervisão de preceptores. Deste modo, simularemos também a atuação do Licenciando, quando egresso do curso, no mundo do trabalho. Esta prática será pautada no aprendizado do Licenciando nas unidades curriculares de Instrumentação para o Ensino de Ciências e Biologia, por meio da utilização de metodologias ativas, TIC's e espaços laboratoriais, efetuando, assim, a integração com as outras áreas do Revisitando nestes espaços pedagógicos.</p>
          <a href="<?= base_url() ?>index.php/welcome/cadastro">
          <div class="text-center pull-right bttn-slant bttn-md bttn-primary cor">Inscreva-se Grátis</div>
          </a> </div>
        <div class="col-sm-1"></div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="">
    <div class="col-md-12 center-block navbar n" style="opacity:1;">
      <div class="center-block apresentacao">
        <h1><strong class="strong-borda texto-area">Por que o Revisitando Unigranrio?</strong></h1>
      </div>
      <div class="row">
        <div class="col-sm-6 pq text-right">
          <h2>#Aproveitamento</h2>
          <p>Reduzir a reprovação nos períodos iniciais, aumentando, assim, a retenção na graduação.</p>
        </div>
        <div class="col-sm-6 pq pq2">
          <h2>#Oportunidade</h2>
          <p>Aumentar a oportunidade de estágio em um cenário interno.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 pq pq2 text-right">
          <h2>#Desempenho</h2>
          <p>Melhorar o desempenho em avaliações externas.</p>
        </div>
        <div class="col-sm-6 pq">
          <h2>#Satisfação</h2>
          <p>Aumentar o nível de satisfação do aluno, potencializando a divulgação.</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!--application/views/rodape.php-->
<?php include_once 'rodape.php' ?>

</body>
</html>
