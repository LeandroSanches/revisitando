<!DOCTYPE html>
<html>
<head>
<?php include 'importacoes.php' ?>
</head>
<body>
<?php include_once 'cabecalho.php' ?>
<section class="section-formulario">
<?php $attributes = array('class' => 'form-horizontal', 'id' => 'myform'); ?>

<?php echo form_open("welcome/validar"); ?>
  <div class="container">
    <div class="row div-formulario">
    	<? if(validation_errors()==''){}

    else{
    	?>
        <div class="alert alert-danger div-erro" role="alert"> <?php echo validation_errors(); ?> </div>
	<? }?>
      <h1 class="tituto_chamada">PREENCHA SEUS DADOS</h1>
      <div class="form-cadastro col-md-12  ">
        <form>
          <div class="form-group">
            <label for="exampleInputEmail1">Nome Completo</label>
            <input value="<?=set_value("nome","")?>" type="text" class="form-control" name="nome" id="nome" placeholder="Nome Completo" autofocus>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">E-mail</label>
            <input value="<?=set_value("email","")?>" type="email" class="form-control" name="email"  id="email" placeholder="E-mail">
          </div>
          <div class="form-group">
            <label for="telefone">Telefone </label>
            <input value="<?=set_value("telefone","")?>" type="tel" class="form-control"  name="telefone" id="telefone" placeholder="Telefone">
          </div>
          <div class="form-group">
            <label for="cpf">CPF (Digite apenas números) </label>
            <input type="text" name="cpf" value="<?=set_value("cpf","")?>" title="Digite o CPF" class="form-control" id="cpf" placeholder="CPF">
          </div>
          <div class="form-group">
            <label for="exampleInputCurso">Área de Prefêrencia: </label>
            <select name="curso" class="select form-control" id="curso" title="Curso Escolhido:">
            <option value="<?=set_value("curso","")?>"   class="select_option" title="Escolhar seu curso">Escolhar seu curso</option>
            <option value="<?=set_value("curso","01")?>" class="select_option" title="História">História</option>
            <option value="<?=set_value("curso","02")?>" class="select_option" title="Quimica">Quimica</option>
            <option value="<?=set_value("curso","03")?>" class="select_option" title="Matemática">Matemática</option>
            <option value="<?=set_value("curso","04")?>" class="select_option" title="Português E Inglês">Português E Inglês</option>
            <option value="<?=set_value("curso","05")?>" class="select_option" title="Ciência E Biologia">Ciência E Biologia</option>
            </select>
          </div>
          <div class="form-group">
            <label for="senha">Senha</label>
            <input type="password"  name="senha" class="form-control" id="senha" placeholder="Senha">
          </div>
          <div class="form-group">
            <label for="senhac">Confirmar senha</label>
            <input type="password"  name="senha2" class="form-control" id="senhac" placeholder="Confirmar senha">
          </div>
          <button type="submit" class="btn btn-success conversao btn-block">Enviar</button>
          <?php echo form_close(); ?>
        </form>
      </div>
    </div>
  </div>
</section>
<!--application/views/rodape.php-->
<?php include_once 'rodape.php' ?>
</body>
</html>
